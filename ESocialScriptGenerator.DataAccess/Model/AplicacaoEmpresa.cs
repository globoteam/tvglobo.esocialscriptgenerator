﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESocialScriptGenerator.DataAccess.Model
{
    public class AplicacaoEmpresa
    {
        public string AppNome { get; set; }
        public string Estado { get; set; }
        public string CodigoEmpresa1 { get; set; }
        public string CodigoEmpresa2 { get; set; }
        public string CodigoEmpresa3 { get; set; }
    }
}
