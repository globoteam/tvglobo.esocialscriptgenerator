﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESocialScriptGenerator.DataAccess.Model
{
    public class Tabela
    {
        public string AppNome { get; set; }
        public string BancoDados { get; set; }
        public string TabelaNome { get; set; }
        public string CampoNome { get; set; }
        public string CampoEmpresa { get; set; }
        public string ValorCampoEmpresa { get; set; }
        public string Formato { get; set; }
        public int Onda { get; set; }
        public string Where { get; set; }
    }
}
