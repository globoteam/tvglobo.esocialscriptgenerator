﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESocialScriptGenerator.DataAccess.Model
{
    public class Empresa
    {
        public string Estado { get; set; }
        public int Codigo { get; set; }
    }
}
