﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESocialScriptGenerator.DataAccess.Model
{
    public class Matricula
    {
        public int EmpresaOrigem { get; set; }
        public string MatriculaOrigem { get; set; }
        public int EmpresaDestino { get; set; }
        public string MatriculaDestino { get; set; }
        public int Onda { get; set; }
    }
}
