﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESocialScriptGenerator.DataAccess.Model
{
    public class Constrainst
    {
        public string AppNome { get; set; }
        public string Nome { get; set; }
        public string TabelaNome { get; set; }
        public string BancoDados { get; set; }
        public string Campos { get; set; }
        public bool Producao { get; set; }
        public bool Homologacao { get; set; }
        public int Onda { get; set; }
    }
}
