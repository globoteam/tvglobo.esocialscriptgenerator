﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESocialScriptGenerator.DataAccess
{
    public class Aplicacao
    {
        public string AppNome { get; set; }
        public string HMLSchema { get; set; }
        public string PRDSchema { get; set; }
        public int Tabelas { get; set; }
        public string BancoDados { get; set; }
    }
}
