﻿using ESocialScriptGenerator.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;

namespace ESocialScriptGenerator.DataAccess
{
    public class Excel
    {
        public DataTable GetTable(string excelPath, string tabSheet)
        {
            DataTable table = new DataTable();
            string connectionString = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 8.0;HDR=Yes;'", excelPath);

            using (var connection = new OleDbConnection(connectionString))
            {
                connection.Open();
                try
                {
                    var command = new OleDbCommand(string.Format("SELECT * FROM [{0}$]", tabSheet), connection);
                    try
                    {
                        var adapter = new OleDbDataAdapter(command);
                        try
                        {
                            adapter.Fill(table);
                        }
                        finally
                        {
                            adapter.Dispose();
                        }
                    }
                    finally
                    {
                        command.Dispose();
                    }
                }
                finally
                {
                    connection.Close();
                }
            }
            return table;
        }

        private int Parser(string value)
        {
            int result = 0;
            Int32.TryParse(value, out result);
            return result;
        }

        public IEnumerable<Aplicacao> GetAplicacoes(string _excelPath)
        {
            DataTable appstable = this.GetTable(_excelPath, "Aplicacoes");
            try
            {
                foreach (DataRow row in appstable.Rows)
                {
                   yield return new Aplicacao()
                    {
                        AppNome = row.ItemArray[0].ToString().Trim(),
                        HMLSchema = row.ItemArray[1].ToString().Trim(),
                        PRDSchema = row.ItemArray[2].ToString().Trim(),
                        Tabelas = Parser(row.ItemArray[3].ToString().Trim()),
                        BancoDados = row.ItemArray[4].ToString().Trim()
                   };
                }
            }
            finally
            {
                appstable.Dispose();
            }
        }

        public IEnumerable<Tabela> GetTabelas(string _excelPath)
        {
            DataTable appstable = this.GetTable(_excelPath, "Tabelas");
            try
            {
                foreach (DataRow row in appstable.Rows)
                {
                    yield return new Tabela()
                    {
                        AppNome = row.ItemArray[0].ToString().Trim(),
                        BancoDados = row.ItemArray[1].ToString().Trim(),
                        TabelaNome = row.ItemArray[2].ToString().Trim(),
                        CampoNome = row.ItemArray[3].ToString().Trim(),
                        CampoEmpresa = row.ItemArray[4].ToString().Trim(),
                        ValorCampoEmpresa = row.ItemArray[5].ToString().Trim(),
                        Formato = row.ItemArray[6].ToString().Trim(),
                        Onda = Parser(row.ItemArray[7].ToString().Trim()),
                        Where = row.ItemArray[8].ToString().Trim(),
                    };
                }
            }
            finally
            {
                appstable.Dispose();
            }
        }

        public IEnumerable<Matricula> GetMatriculas(string _excelPath)
        {
            DataTable appstable = this.GetTable(_excelPath, "Matriculas");
            try
            {
                foreach (DataRow row in appstable.Rows)
                {
                    yield return new Matricula()
                    {
                        EmpresaOrigem = Parser(row.ItemArray[0].ToString().Trim()),
                        MatriculaOrigem = row.ItemArray[1].ToString().Trim(),
                        EmpresaDestino = Parser(row.ItemArray[2].ToString().Trim()),
                        MatriculaDestino = row.ItemArray[3].ToString().Trim(),
                        Onda = Parser(row.ItemArray[4].ToString().Trim())

                    };
                }
            }
            finally
            {
                appstable.Dispose();
            }

        }


        public IEnumerable<Constrainst> GetConstrainst(string _excelPath)
        {
            DataTable appstable = this.GetTable(_excelPath, "FK");
            try
            {
                foreach (DataRow row in appstable.Rows)
                {
                    yield return new Constrainst()
                    {

                        AppNome = row.ItemArray[0].ToString().Trim(),
                        BancoDados = row.ItemArray[1].ToString().Trim(),
                        TabelaNome  = row.ItemArray[2].ToString().Trim(),
                        Nome = row.ItemArray[3].ToString().Trim(),
                        Campos = row.ItemArray[4].ToString().Trim(),
                        Producao  = row.ItemArray[5].ToString().Trim().Length > 0,
                        Homologacao = row.ItemArray[6].ToString().Trim().Length > 0,
                        Onda = Parser(row.ItemArray[7].ToString().Trim())
                    };
                }
            }
            finally
            {
                appstable.Dispose();
            }
        }


        public IEnumerable<Constrainst> GetPK(string _excelPath)
        {
            DataTable appstable = this.GetTable(_excelPath, "PK");
            try
            {
                foreach (DataRow row in appstable.Rows)
                {
                    yield return new Constrainst()
                    {

                        AppNome = row.ItemArray[0].ToString().Trim(),
                        BancoDados = row.ItemArray[1].ToString().Trim(),
                        TabelaNome = row.ItemArray[2].ToString().Trim(),
                        Nome = row.ItemArray[3].ToString().Trim(),
                        Campos = row.ItemArray[4].ToString().Trim(),
                        Producao = row.ItemArray[5].ToString().Trim().Length > 0,
                        Homologacao = row.ItemArray[6].ToString().Trim().Length > 0,
                        Onda = Parser(row.ItemArray[7].ToString().Trim())
                    };
                }
            }
            finally
            {
                appstable.Dispose();
            }
        }

        public IEnumerable<Empresa> GetEmpresas(string _excelPath)
        {
            DataTable appstable = this.GetTable(_excelPath, "Empresas");
            try
            {
                foreach (DataRow row in appstable.Rows)
                {
                    yield return new Empresa()
                    {
                        Estado = row.ItemArray[0].ToString().Trim(),
                        Codigo = Parser(row.ItemArray[1].ToString().Trim())
                    };
                }
            }
            finally
            {
                appstable.Dispose();
            }
        }

        public IEnumerable<AplicacaoEmpresa> GetAplicacaoEmpresas(string _excelPath)
        {
            DataTable appstable = this.GetTable(_excelPath, "AppxEmpresa");
            try
            {
                foreach (DataRow row in appstable.Rows)
                {
                    yield return new AplicacaoEmpresa()
                    {
                        AppNome = row.ItemArray[0].ToString().Trim(),
                        Estado = row.ItemArray[1].ToString().Trim(),
                        CodigoEmpresa1 = row.ItemArray[2].ToString().Trim(),
                        CodigoEmpresa2 = row.ItemArray[3].ToString().Trim(),
                        CodigoEmpresa3 = row.ItemArray[4].ToString().Trim()
                    };
                }
            }
            finally
            {
                appstable.Dispose();
            }
        }
    }
}
