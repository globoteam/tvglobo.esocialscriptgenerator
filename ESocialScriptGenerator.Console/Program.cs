﻿using CommandLine;
using CommandLine.Text;
using ESocialScriptGenerator.Business;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ESocialScriptGenerator
{
    class Options
    {
        [Option('e', "excelFileName", Required = true,
          HelpText = "Caminho completo do arquivo excel")]
        public string excelFileName { get; set; }


        [Option('t', "template", Required = true,
          HelpText = "Caminho dos arquivos de templates.")]
        public string pathTemplate { get; set; }

        [Option('o', "output", Required = true,
          HelpText = "Caminho de saida para os arquivos que serão criados.")]
        public string pathOutput { get; set; }

        [Option('a', "ambiente", Required = true, 
          HelpText = "Informe o ambiente: 1 - Produção; 2- Homologação")]
        public string ambiente { get; set; }

        [Option('n', "onda", Required = true,
          HelpText = "Informe o número da onda")]
        public string onda { get; set; }

        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var options = new Options();
            if (CommandLine.Parser.Default.ParseArguments(args, options))
            {

                try
                {
                    Console.WriteLine();
                    Console.WriteLine();
                    if (!File.Exists(options.excelFileName))
                    {
                        throw new Exception(String.Format("O arquivo {0} não foi encontrado.", options.excelFileName));
                    }
                    if (!Directory.Exists(options.pathTemplate))
                    {
                        throw new Exception(String.Format("O path {0} não foi encontrado.", options.pathTemplate));
                    }

                    int ambiente = 0;
                    int.TryParse(options.ambiente, out ambiente);
                    if(ambiente <= 0)
                    {
                        throw new Exception(String.Format("O ambiente não {0} existe.", options.ambiente));
                    }

                    Convert.ToInt32(options.onda);

                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.WriteLine("Parâmetros informados");
                    Console.WriteLine("Excel Filename: {0}", options.excelFileName);
                    Console.WriteLine("Template path: {0}", options.pathTemplate);
                    Console.WriteLine("Output path: {0}", options.pathOutput);
                    Console.WriteLine("Onda: {0}", options.onda);

                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.Write("Transformação iniciada...");
                    ScriptBusiness business = new ScriptBusiness(options.excelFileName, options.pathOutput, options.pathTemplate, Convert.ToInt32(options.onda), (Ambiente)ambiente);
                    business.Generate();
                    Console.ForegroundColor = ConsoleColor.DarkBlue;
                    Console.Write("[Concluida]");

                    Console.WriteLine();
                    Console.WriteLine();
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    Console.WriteLine("Arquivos disponíveis em: ");
                    Console.WriteLine(options.pathOutput);

                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("Ocorreu o seguinte erro: ");
                    Console.WriteLine();
                    Console.WriteLine(ex.Message);
                    if (ex.InnerException != null)
                    {
                        Console.WriteLine();
                        Console.WriteLine(ex.InnerException.Message);
                    }
                }
                finally
                {
                    Console.ResetColor();
                }
            }
        }
    }
}
