﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ESocialScriptGenerator.Business;
using System.IO;
using System.Linq;

namespace ESocialScriptGenerator.Test
{
    [TestClass]
    public class GenerateScriptTest
    {
        [TestMethod]
        public void GenerateScriptsOnda1_Producao()
        {
            string excelPath = @"D:\Projetos\ESocial\branch\tvglobo.esocialscriptgenerator\ESocialScriptGenerator.Test\bin\Debug\Matricula- De-Para.xlsx";
            string outputPath = @"D:\Projetos\ESocial\branch\tvglobo.esocialscriptgenerator\ESocialScriptGenerator.Test\bin\Debug\output";
            string templatePath = @"D:\Projetos\ESocial\branch\tvglobo.esocialscriptgenerator\ESocialScriptGenerator.Test\bin\Debug\Template";
            int onda = 1;
            ScriptBusiness business = new ScriptBusiness(excelPath, outputPath, templatePath, onda, Ambiente.Producao);
            business.Generate();
        }

        [TestMethod]
        public void GenerateScriptsOnda2_Producao()
        {
            string excelPath = @"D:\Projetos\ESocial\branch\tvglobo.esocialscriptgenerator\ESocialScriptGenerator.Test\bin\Debug\Matricula- De-Para.xlsx";
            string outputPath = @"D:\Projetos\ESocial\branch\tvglobo.esocialscriptgenerator\ESocialScriptGenerator.Test\bin\Debug\output";
            string templatePath = @"D:\Projetos\ESocial\branch\tvglobo.esocialscriptgenerator\ESocialScriptGenerator.Test\bin\Debug\Template";
            int onda = 2;
            ScriptBusiness business = new ScriptBusiness(excelPath, outputPath, templatePath, onda, Ambiente.Producao);
            business.Generate();
        }

        [TestMethod]
        public void GenerateScriptsOnda2_Homologacao()
        {
            string excelPath = @"D:\Projetos\ESocial\branch\tvglobo.esocialscriptgenerator\ESocialScriptGenerator.Test\bin\Debug\Matricula- De-Para.xlsx";
            string outputPath = @"D:\Projetos\ESocial\branch\tvglobo.esocialscriptgenerator\ESocialScriptGenerator.Test\bin\Debug\output";
            string templatePath = @"D:\Projetos\ESocial\branch\tvglobo.esocialscriptgenerator\ESocialScriptGenerator.Test\bin\Debug\Template";
            int onda = 2;
            ScriptBusiness business = new ScriptBusiness(excelPath, outputPath, templatePath, onda, Ambiente.Homologacao);
            business.Generate();
        }


        [TestMethod]
        public void GenerateScriptsOnda3_Producao()
        {
            string excelPath = @"D:\Projetos\ESocial\branch\tvglobo.esocialscriptgenerator\ESocialScriptGenerator.Test\bin\Debug\Matricula- De-Para.xlsx";
            string outputPath = @"D:\Projetos\ESocial\branch\tvglobo.esocialscriptgenerator\ESocialScriptGenerator.Test\bin\Debug\output";
            string templatePath = @"D:\Projetos\ESocial\branch\tvglobo.esocialscriptgenerator\ESocialScriptGenerator.Test\bin\Debug\Template";
            int onda = 3;
            ScriptBusiness business = new ScriptBusiness(excelPath, outputPath, templatePath, onda, Ambiente.Producao);
            business.Generate();
        }

        [TestMethod]
        public void GenerateScriptsOnda3_Homologacao()
        {
            string excelPath = @"D:\Projetos\ESocial\branch\tvglobo.esocialscriptgenerator\ESocialScriptGenerator.Test\bin\Debug\Matricula- De-Para.xlsx";
            string outputPath = @"D:\Projetos\ESocial\branch\tvglobo.esocialscriptgenerator\ESocialScriptGenerator.Test\bin\Debug\output";
            string templatePath = @"D:\Projetos\ESocial\branch\tvglobo.esocialscriptgenerator\ESocialScriptGenerator.Test\bin\Debug\Template";
            int onda = 3;
            ScriptBusiness business = new ScriptBusiness(excelPath, outputPath, templatePath, onda, Ambiente.Homologacao);
            business.Generate();
        }


        [TestMethod]
        public void CheckResultHEE()
        {

            var textFinal = String.Empty;

            var filenames = Directory.GetFiles(@"C:\Users\ricardors\Downloads\exec_hee.log\", "*.log", SearchOption.AllDirectories);
            foreach (var filename in filenames)
            {
                string lines = File.ReadAllText(filename);

                while (lines.Length > 0) {

                    int startPosition = lines.IndexOf("== INICIO UDPATE HEEP ==");
                    if (startPosition < 0) return;

                    lines = lines.Substring(startPosition + 25);
                    startPosition = lines.IndexOf("== INICIO UDPATE HEEP ==");
                    lines = lines.Substring(startPosition + 25);

                    var linesAnalized = lines.Substring(0);
                    int endPosition = linesAnalized.IndexOf("== FIM HEEP ==");
                    linesAnalized = linesAnalized.Substring(0, endPosition);

                    while (linesAnalized.Length > 0)
                    {

                        int line1EndPosition = linesAnalized.IndexOf("\n");
                        var line1 = linesAnalized.Substring(0, line1EndPosition);
                        linesAnalized = linesAnalized.Substring(line1EndPosition + 1);

                        int line2EndPosition = linesAnalized.IndexOf("\n");
                        var line2 = linesAnalized.Substring(0, line2EndPosition);
                        linesAnalized = linesAnalized.Substring(line2EndPosition + 1);

                        int line3EndPosition = 0;
                        if (line2.IndexOf("COMMIT: NOK") >= 0)
                        {
                            var conteudoSel1 = line1.Split(new[] { ":", "->", " " }, StringSplitOptions.RemoveEmptyEntries);
                            var conteudoSel2 = line2.Split(new[] { ":" }, StringSplitOptions.RemoveEmptyEntries);


                            line3EndPosition = linesAnalized.IndexOf("\n");
                            var line3 = linesAnalized.Substring(0, line3EndPosition);

                            textFinal += conteudoSel1[0] + ": " + conteudoSel1[1] + " -> " + line3 + Environment.NewLine;
                        } else
                        {
                            line3EndPosition = linesAnalized.IndexOf("\n");
                        }

                        linesAnalized = linesAnalized.Substring(line3EndPosition + 1);
                    }
                }
            }
        }
    }
}
