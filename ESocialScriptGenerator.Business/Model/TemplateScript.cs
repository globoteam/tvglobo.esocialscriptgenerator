﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESocialScriptGenerator.Business.Model
{
    public class TemplateScript
    {
        public TemplateScript(string bancoDados)
        {
            if(bancoDados.ToLower() == "oracle")
            {
                PKEnable = "ScriptEnablePKOracle.tpl";
                PKDisable = "ScriptDisablePKOracle.tpl";
                ConstraintEnable = "ScriptEnableConstraintOracle.tpl";
                ConstraintDisable = "ScriptDisableConstraintOracle.tpl";
                Matricula = "ScriptMatriculaOracle.tpl";
                Check = "ScriptCheckOracle.tpl";
                Run = "ScriptRunOracle.tpl";
            }
            else if (bancoDados.ToLower() == "sql server")
            {
                PKEnable = "ScriptEnablePKSQLServer.tpl";
                PKDisable = "ScriptDisablePKSQLServer.tpl";
                ConstraintEnable = "ScriptEnableConstraintSQLServer.tpl";
                ConstraintDisable = "ScriptDisableConstraintSQLServer.tpl";
                Matricula = "ScriptMatriculaSQLServer.tpl";
                Check = "ScriptCheckSQLServer.tpl";
            }
        }

        public string PKEnable { get; set; }
        public string PKDisable { get; set; }
        public string ConstraintEnable { get; set; }
        public string ConstraintDisable { get; set; }
        public string Matricula { get; set; }
        public string Check { get; set; }
        public string Run { get; set; }
    }
}
