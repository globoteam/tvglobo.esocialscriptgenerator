﻿using DotLiquid;
using ESocialScriptGenerator.Business.Model;
using ESocialScriptGenerator.DataAccess;
using ESocialScriptGenerator.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace ESocialScriptGenerator.Business
{
    public enum Ambiente {
        None = 0,
        Producao = 1,
        Homologacao = 2
    }

    public class ScriptBusiness
    {
        private Excel _excel = new Excel();
        private string _excelPath = string.Empty;
        private string _outputPath = string.Empty;
        private string _templatePath = string.Empty;
        private IList<string> _scriptFiles = new List<String>();
        private int _onda = -1;
        private Ambiente _ambiente = Ambiente.None;
        private IDictionary<string, AplicacaoEmpresa> _cacheEmpresa = new Dictionary<string, AplicacaoEmpresa>();
        public ScriptBusiness(string excelPath, string outputPath, string templatePath, int onda, Ambiente ambiente)
        {
            _excelPath = excelPath;
            _outputPath = outputPath;
            _templatePath = templatePath;
            _onda = onda;
            _ambiente = ambiente;

            if (_outputPath.LastIndexOf("\\") <= 0)
            {
                _outputPath = "\\";
            };
            if (!Directory.Exists(_outputPath))
            {
                Directory.CreateDirectory(_outputPath);
            }
        }

        public void Generate()
        {
            IEnumerable<Aplicacao> listApps = _excel.GetAplicacoes(_excelPath);
            foreach (Aplicacao app in listApps)
            {
                _scriptFiles.Clear();
                var templateScript = new TemplateScript(app.BancoDados);
                GenerateScriptMatriculas(app, templateScript);
                if (app.BancoDados.ToLower() == "oracle")
                {
                    var schema = GetSchema(app);
                    string path = GetPath(app.AppNome);
                    string fileName = Path.Combine(path, "0-Run.sql");
                    DeleteFile(fileName);

                    if (_scriptFiles.Count > 0)
                    {
                        var files = _scriptFiles.ToList();
                        files.Sort();
                        SaveScriptFile(fileName, templateScript.Run, (file, tpl) =>
                        {
                            return new
                            {
                                files = files,
                                schema_nome = schema
                            };
                        });
                    }
                }
            }
            
        }

        private void DeleteFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
        }

        private string GetPath(string app_name)
        {
            return Path.Combine(_outputPath, app_name + "\\");
        }

        private void GenerateScriptCheck(Aplicacao app, TemplateScript template, List<dynamic> list, string filename, bool isStart)
        {
            var schema = GetSchema(app);

            var matriculas = list.Where(x => x.matricula_origem != String.Empty && x.matricula_destino != String.Empty)
                                 .GroupBy(x => new { Empresa_Codigo = x.empresa_codigo, MatriculaOrigem = x.matricula_origem, MatriculaDestino = x.matricula_destino })
                                 .Select(x => new { Empresa_Codigo = x.Key.Empresa_Codigo, MatriculaOrigem = x.Key.MatriculaOrigem, MatriculaDestino = x.Key.MatriculaDestino }).ToList();

            var listTabelas = list.Where(x => x.tabela_nome != String.Empty && x.empresa_campo != String.Empty && x.empresa_codigo != String.Empty)
                                  .GroupBy(x => new { x.tabela_nome, x.campo_nome, x.empresa_campo, x.empresa_codigo})
                                  .Select(x => new { tabela_nome = x.Key.tabela_nome, campo_nome = x.Key.campo_nome, empresa_campo = x.Key.empresa_campo, empresa_codigo = x.Key.empresa_codigo })
                                  .ToList();

            IEnumerable<dynamic> listMatriculas = null;

            var listModel = new List<dynamic>();
            if (isStart) {
                listMatriculas = matriculas.Select(x => new { x.Empresa_Codigo, Matricula = x.MatriculaOrigem }).ToList();
            } else
            {
                listMatriculas = matriculas.Select(x => new { x.Empresa_Codigo, Matricula = x.MatriculaDestino }).ToList();
            }

            foreach (var item in listTabelas)
            {
                var listMatriculasSelected = listMatriculas
                    .Where(x => x.Empresa_Codigo == item.empresa_codigo)
                    .Select(x => x.Matricula).ToList();

                string where = item.campo_nome + " in ('" + string.Join("','", listMatriculasSelected) + "')";
                if(item.empresa_codigo.IndexOf(";") >= 0)
                {
                    where = item.empresa_campo.Trim().Length > 0 ? string.Format(" {0} in ('{1}') ", item.empresa_campo, item.empresa_codigo.Replace(";", "','")) + " and (" + where + ") " : where;
                }
                else
                {
                    where = item.empresa_campo.Trim().Length > 0 ? string.Format(" {0} = '{1}' ", item.empresa_campo, item.empresa_codigo) + " and (" + where + ") " : where;
                }

                dynamic model = new
                {
                    tabela_nome = item.tabela_nome.ToString(),
                    empresa_codigo = item.empresa_codigo,
                    empresa_campo = item.empresa_campo,
                    campo_nome = item.campo_nome,
                    where = where
                };
              
                listModel.Add(model);
            }

            var path = GetPath(app.AppNome);
            if (listModel.Count > 0)
            {
                var templateFileName = GetTemplateCheckFileName(app, template);

                var index = 1;
                var limitTake = 50;
                for (int i = 0; i < listModel.Count; i = i + limitTake)
                {
                    var listFiles = listModel.Skip(i).Take(Math.Min(limitTake, listModel.Count));

                    string fileName = Path.Combine(path, String.Format(filename + ".{0}-Check_", index)) + app.AppNome + ".sql";
                    DeleteFile(fileName);

                    SaveScriptFile(fileName, templateFileName, (file, tpl) =>
                    {
                        return new
                        {
                            list = listFiles,
                            schema_nome = schema
                        };
                    });

                    index++;

                }
                
            }

        }

        private void GenerateScriptFK(Aplicacao app, TemplateScript template)
        {
            var schema = GetSchema(app);

            IEnumerable<Constrainst> constrainsts = _excel.GetConstrainst(_excelPath);
            var query = constrainsts.Where(x => x.AppNome == app.AppNome && x.Onda <= _onda && x.Onda > 0).AsQueryable();
            if (_ambiente == Ambiente.Homologacao)
            {
                query = query.Where(x => x.Homologacao == true);
            }
            else if (_ambiente == Ambiente.Producao)
            {
                query = query.Where(x => x.Producao == true);
            }
            var selectRows = query.ToList();

            var list = new List<dynamic>();
            foreach (Constrainst row in selectRows)
            {
                dynamic model = new
                {
                    app_nome = row.AppNome,
                    banco_dados = row.BancoDados,
                    tabela_nome = row.TabelaNome,
                    constrainst_nome = row.Nome,
                    campo_nome = row.Campos
                };
                list.Add(model);
            }
            var path = GetPath(app.AppNome);

            string fileName = Path.Combine(path, "2-FK_Disable_") + app.AppNome + ".sql";
            DeleteFile(fileName);
            if (list.Count > 0)
            {
                SaveScriptFile(fileName, template.ConstraintDisable, (file, tpl) =>
                {
                    return new { list = list,
                        schema_nome = schema
                    };
                });
            }

            fileName = Path.Combine(path, "5-FK_Enable_") + app.AppNome + ".sql";
            DeleteFile(fileName);
            if (list.Count > 0)
            {
                SaveScriptFile(fileName, template.ConstraintEnable, (file, tpl) =>
                {
                    return new
                    {
                        list = list,
                        schema_nome = schema
                    };
                });
            }

        }

        private string GetSchema(Aplicacao app)
        {
            return _ambiente == Ambiente.Homologacao ? app.HMLSchema : app.PRDSchema;
        }


        private void GenerateScriptPK(Aplicacao app, TemplateScript template)
        {
            IEnumerable<Constrainst> pks = _excel.GetPK(_excelPath);
            var schema = GetSchema(app);

            var query = pks.Where(x => x.AppNome == app.AppNome && x.Onda <= _onda && x.Onda > 0).AsQueryable();
            if (_ambiente == Ambiente.Homologacao)
            {
                query = query.Where(x => x.Homologacao == true);
            } else if (_ambiente == Ambiente.Producao)
            {
                query = query.Where(x => x.Producao == true);
            }

            var selectRows = query.ToList();
            var list = new List<dynamic>();
            foreach (Constrainst row in selectRows)
            {
                dynamic model = new
                {
                    app_nome = row.AppNome,
                    banco_dados = row.BancoDados,
                    tabela_nome = row.TabelaNome,
                    constrainst_nome = row.Nome,
                    campo_nome = row.Campos
                };
                list.Add(model);
            }
            var path = GetPath(app.AppNome);

            string fileName = Path.Combine(path, "2-PK_Disable_") + app.AppNome + ".sql";
            DeleteFile(fileName);
            if (list.Count > 0)
            {
                SaveScriptFile(fileName, template.PKDisable, (file, tpl) =>
                {
                    return new { list = list,
                        schema_nome = schema
                    };
                });
            }

            fileName = Path.Combine(path, "5-PK_Enable_") + app.AppNome + ".sql";
            DeleteFile(fileName);

            if (list.Count > 0)
            {
                SaveScriptFile(fileName, template.PKEnable, (file, tpl) =>
                {
                    return new
                    {
                        list = list,
                        schema_nome = schema
                    };
                });
            }

        }

        private AplicacaoEmpresa GetCodigoEmpresa(string AppNome, Matricula matricula, IEnumerable<Empresa> empresas, IEnumerable<AplicacaoEmpresa> appxempresa)
        {
            string key = AppNome + "_" + matricula.EmpresaOrigem;
            bool isContainKey = _cacheEmpresa.ContainsKey(key);
            AplicacaoEmpresa appemp = isContainKey ? _cacheEmpresa[key] : null;
            if(!isContainKey)
            {
                Empresa empresa = empresas.Where(x => x.Codigo == matricula.EmpresaOrigem).FirstOrDefault();
                if(empresa != null)
                    appemp = appxempresa.Where(x => x.AppNome == AppNome && x.Estado == empresa.Estado).FirstOrDefault();

                _cacheEmpresa[key] = appemp;
            }
            return appemp;
        }


        private string GetTemplateCheckFileName(Aplicacao app, TemplateScript template)
        {
            string templateFileName = template.Check;
            if (File.Exists(Path.Combine(_templatePath, string.Format("ScriptCheck{0}.tpl", app.AppNome))))
            {
                templateFileName = string.Format("ScriptCheck{0}.tpl", app.AppNome);
            }
            return templateFileName;
        }

        private string GetTemplateMatriculaFileName(Aplicacao app, TemplateScript template)
        {
            string templateFileName = template.Matricula;
            if (File.Exists(Path.Combine(_templatePath, string.Format("ScriptMatricula{0}.tpl", app.AppNome))))
            {
                templateFileName = string.Format("ScriptMatricula{0}.tpl", app.AppNome);
            }
            return templateFileName;
        }

        
        private void GenerateScriptMatriculas(Aplicacao app, TemplateScript template)
        {
            string schema = GetSchema(app);
            string templateFileName = GetTemplateMatriculaFileName(app, template);

            IEnumerable<Tabela> tabelas = _excel.GetTabelas(_excelPath);
            IEnumerable<Matricula> matriculas = _excel.GetMatriculas(_excelPath);
            IEnumerable<Empresa> empresas = _excel.GetEmpresas(_excelPath);
            IEnumerable<AplicacaoEmpresa> appxempresas = _excel.GetAplicacaoEmpresas(_excelPath);

            var selectMatriculas = matriculas.Where(x => x.Onda <= _onda && x.Onda > 0);

            var selectRowsTabelas = tabelas.Where(x => x.AppNome == app.AppNome && x.Onda <= _onda && x.Onda > 0);
            if (selectRowsTabelas.Count() == 0)
            {
                return;
            }

            var path = GetPath(app.AppNome);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            
            var list = new List<dynamic>();
            foreach (Tabela tabela in selectRowsTabelas)
            {
                bool isFormatar = tabela.Formato.Trim().ToLower().Contains("formatar");
                bool isConcatenado = (tabela.Formato.Trim().ToLower().CompareTo("concatenado") == 0);
                bool isContains = (tabela.Formato.Trim().ToLower().CompareTo("contains") == 0);
                bool isColuna2 = tabela.ValorCampoEmpresa.ToLower().Trim() == "coluna 2";
                bool isColuna3 = tabela.ValorCampoEmpresa.ToLower().Trim() == "coluna 3";

                string formatvalue = String.Empty;
                int formatlength = 0;
                if (isFormatar)
                {
                    var formatText = tabela.Formato.Trim().ToLower().Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    formatvalue = formatText[1];
                    int.TryParse(formatText[2], out formatlength);
                }

                foreach (Matricula matricula in selectMatriculas)
                {
                    AplicacaoEmpresa appempresa = GetCodigoEmpresa(app.AppNome, matricula, empresas, appxempresas);
                    if (appempresa == null) continue;

                    string codigoEmpresaOrigem = appempresa.CodigoEmpresa1;
                    if (isColuna2 && appempresa.CodigoEmpresa2.Trim().Length > 0)
                    {
                        codigoEmpresaOrigem = appempresa.CodigoEmpresa2;
                    } else if (isColuna3 && appempresa.CodigoEmpresa2.Trim().Length > 0)
                    {
                        codigoEmpresaOrigem = appempresa.CodigoEmpresa3;
                    }

                    string matricula_origem = isConcatenado ? codigoEmpresaOrigem + matricula.MatriculaOrigem.ToString() : matricula.MatriculaOrigem.ToString();
                    string matricula_destino = isConcatenado ? codigoEmpresaOrigem + matricula.MatriculaDestino.ToString() : matricula.MatriculaDestino.ToString();

                    if (isFormatar)
                    {
                        matricula_origem = matricula_destino.PadLeft(formatlength, formatvalue.ToCharArray()[0]);
                        matricula_destino = matricula_destino.PadLeft(formatlength, formatvalue.ToCharArray()[0]);
                    }

                    string where = String.Empty;
                    string whereDepois = String.Empty;
                    if (isContains)
                    {
                        where = tabela.CampoEmpresa.Trim().Length > 0 ?
                        string.Format(" {0} = '{1}' and {2} in ('{3}') ", tabela.CampoNome, matricula_origem, tabela.CampoEmpresa, codigoEmpresaOrigem.Replace(";", "','")) :
                        string.Format(" {0} = '{1}' {2} ", tabela.CampoNome, matricula_origem, string.Format(tabela.Where, codigoEmpresaOrigem));

                        whereDepois = tabela.CampoEmpresa.Trim().Length > 0 ?
                           string.Format(" {0} = '{1}' and {2} in ('{3}') ", tabela.CampoNome, matricula_destino, tabela.CampoEmpresa, codigoEmpresaOrigem.Replace(";", "','")) :
                           string.Format(" {0} = '{1}' {2} ", tabela.CampoNome, matricula_destino, string.Format(tabela.Where, codigoEmpresaOrigem));

                    }
                    else
                    {
                        where = tabela.CampoEmpresa.Trim().Length > 0 ?
                            string.Format(" {0} = '{1}' and {2} = '{3}' ", tabela.CampoNome, matricula_origem, tabela.CampoEmpresa, codigoEmpresaOrigem) :
                            string.Format(" {0} = '{1}' {2} ", tabela.CampoNome, matricula_origem, string.Format(tabela.Where, codigoEmpresaOrigem));

                        whereDepois = tabela.CampoEmpresa.Trim().Length > 0 ?
                           string.Format(" {0} = '{1}' and {2} = '{3}' ", tabela.CampoNome, matricula_destino, tabela.CampoEmpresa, codigoEmpresaOrigem) :
                           string.Format(" {0} = '{1}' {2} ", tabela.CampoNome, matricula_destino, string.Format(tabela.Where, codigoEmpresaOrigem));

                    }
                    dynamic model = new
                    {
                        schema_nome = schema,
                        app_nome = tabela.AppNome.ToString(),
                        tabela_nome = tabela.TabelaNome.ToString(),
                        campo_nome = tabela.CampoNome.ToString(),
                        empresa_codigo = codigoEmpresaOrigem,
                        empresa_campo = tabela.CampoEmpresa,
                        matricula_origem = matricula_origem,
                        matricula_destino = matricula_destino,
                        where = where,
                        where_depois = whereDepois
                    };

                    list.Add(model);
                }

            }

            GenerateScriptCheck(app, template, list, "1", true);
            if (list.Count > 0)
            {
                var index = 1;
                var limitTake = 100;
                for (int i = 0; i < list.Count; i = i + limitTake)
                {
                    var listFiles = list.Skip(i).Take(Math.Min(limitTake, list.Count));

                    string fileName = Path.Combine(path, String.Format("3.{0}-Update_", index)) + app.AppNome + ".sql";
                    DeleteFile(fileName);

                    SaveScriptFile(fileName, templateFileName, (file, tpl) =>
                    {
                        return new
                        {
                            list = listFiles,
                            schema_nome = schema
                        };
                    });

                    index++;

                }
            }

            GenerateScriptFK(app, template);
            GenerateScriptPK(app, template);
            GenerateScriptCheck(app, template, list, "4", false);
        }

        private void SaveScriptFile(string outputFileName, string templateName, Func<StreamWriter, Template, object> func)
        {
            using (StreamWriter file = File.AppendText(outputFileName))
            {
                _scriptFiles.Add(new FileInfo(outputFileName).Name);

                TransformTemplate(templateName, (Template tpl) => {               
                    dynamic model = func(file, tpl);
                    string text = tpl.Render(DotLiquid.Hash.FromAnonymousObject(new { info = model }));
                    file.WriteLine(text);
                });
            }
        }

        private void TransformTemplate(string templateName, Action<Template> func)
        {
            using (StreamReader file = File.OpenText(Path.Combine(_templatePath, templateName)))
            {
                Template tpl = Template.Parse(file.ReadToEnd());
                func(tpl);
            }            
        }
    }
}
