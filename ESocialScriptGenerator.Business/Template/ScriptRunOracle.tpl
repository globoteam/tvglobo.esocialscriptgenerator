﻿ALTER SESSION SET CURRENT_SCHEMA= {{info.schema_nome}}
/
set serveroutput on size 100000
set echo on
/
spool execucao_{{info.schema_nome}}.txt
/
{% for item in info.files -%}

@{{item}}
{% endfor -%}
spool off
/
