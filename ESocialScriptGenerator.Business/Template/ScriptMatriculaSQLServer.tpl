﻿SET NOCOUNT ON;  
GO

DECLARE @NCOUNT_ANTES int;
DECLARE @NCOUNT_DEPOIS int;
DECLARE @FAZCOMMIT int;

SET @NCOUNT_ANTES = 0;
SET @NCOUNT_DEPOIS = 0;
SET @FAZCOMMIT = 1;--//1- faz commit; 2-Não faz commit;

PRINT N'== INICIO {{info.schema_nome}} ==';

{% for item in info.list -%}

BEGIN TRANSACTION;
	select @NCOUNT_ANTES = count(1) 
	from  {{item.tabela_nome}} where {{item.where}};
	PRINT N'{{item.tabela_nome}}: {{item.matricula_origem}} -> '+ cast(@NCOUNT_ANTES as varchar(9))  + ' Registros encontrados';

	update {{item.tabela_nome}} set {{item.campo_nome}} = '{{item.matricula_destino}}' where {{item.where}};

	select @NCOUNT_DEPOIS = count(1) 
	from  {{item.tabela_nome}} where {{item.where_depois}};
	PRINT N'{{item.tabela_nome}}: {{item.matricula_destino}} -> '+ cast(@NCOUNT_DEPOIS as varchar(9))  + ' Registros atualizados';

IF @@ERROR = 0
BEGIN
	COMMIT;
	PRINT N'COMMIT: OK';
END ELSE BEGIN
	ROLLBACK;
	PRINT N'COMMIT: NOK';
END;
{% endfor -%}

PRINT N'== FIM {{info.schema_nome}} ==';
GO
SET NOCOUNT OFF;  
GO 