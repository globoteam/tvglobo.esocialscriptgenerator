﻿ALTER SESSION SET CURRENT_SCHEMA= {{info.schema_nome}}
/

select tab.tabela, tab.matricula, tab.empresa, sum(tab.qtdregistros) from (
{% for item in info.list -%}
select '{{item.tabela_nome}}' as Tabela, cast({{item.empresa_campo}} as varchar(20)) as empresa, cast({{item.campo_nome}} as varchar(20)) as matricula,
count(1) as qtdregistros 
from  {{info.schema_nome}}.{{item.tabela_nome}}  where {{item.where}} group by {{item.empresa_campo}}, {{item.campo_nome}}
{%  if forloop.last != true %} UNION {% endif %}{% endfor -%}) tab
group by tab.tabela, tab.empresa, tab.matricula;
