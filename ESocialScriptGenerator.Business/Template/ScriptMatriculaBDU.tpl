﻿SET NOCOUNT ON;  
GO

DECLARE @NCOUNT_ANTES int;
DECLARE @NCOUNT_DEPOIS int;
DECLARE @FAZCOMMIT int;

SET @NCOUNT_ANTES = 0;
SET @NCOUNT_DEPOIS = 0;
SET @FAZCOMMIT = 1;--//1- faz commit; 2-Não faz commit;

PRINT N'== INICIO {{info.schema_nome}} ==';

{% for item in info.list -%}
BEGIN TRANSACTION;

	select @NCOUNT_ANTES = count(1) 
	from  {{item.tabela_nome}} where {{item.campo_nome}} = '{{item.matricula_origem}}' and {{item.empresa_campo}} = '{{item.empresa_codigo}}';
	PRINT N'{{item.tabela_nome}}: {{item.matricula_origem}} -> '+ cast(@NCOUNT_ANTES as varchar(9))  + ' Registros encontrados';

	update TBduUsu0 set CT_SYNC_BDU_AD = (CASE WHEN LEN(ISNULL(NU_AD_SID, '')) > 0 THEN 'U' ELSE 'A' END) where nu_cpf = (select top 1 x.nu_cpf from TBduUtv0 x where x.nu_matr_glb = '{{item.matricula_origem}}' and x.cd_emp_glb = '{{item.empresa_codigo}}') and nu_seq_cpf = (select top 1 x.nu_seq_cpf from TBduUtv0 x where x.nu_matr_glb = '{{item.matricula_origem}}'  and x.cd_emp_glb = '{{item.empresa_codigo}}');
	update {{item.tabela_nome}} set {{item.campo_nome}} = '{{item.matricula_destino}}' where {{item.campo_nome}} = '{{item.matricula_origem}}' and {{item.empresa_campo}} = '{{item.empresa_codigo}}';

	select @NCOUNT_DEPOIS = count(1) 
	from  {{item.tabela_nome}} where {{item.campo_nome}} = '{{item.matricula_destino}}' and {{item.empresa_campo}} = '{{item.empresa_codigo}}';
	PRINT N'{{item.tabela_nome}}: {{item.matricula_destino}} -> '+ cast(@NCOUNT_DEPOIS as varchar(9))  + ' Registros atualizados';

IF @@ERROR = 0
BEGIN
	COMMIT TRANSACTION;
	PRINT N'COMMIT: OK';
END ELSE BEGIN
	ROLLBACK;
	PRINT N'COMMIT: NOK';
END;

{% endfor -%}

PRINT N'== FIM {{info.schema_nome}} ==';
GO
SET NOCOUNT OFF;  
GO 
