﻿ALTER SESSION SET CURRENT_SCHEMA= {{info.schema_nome}}
/
set serveroutput on size 100000;
/
DECLARE
	NCOUNT_ANTES INTEGER := 0;
	NCOUNT_DEPOIS INTEGER := 0;
	FAZCOMMIT INTEGER := 1;--//1- faz commit; 2-Não faz commit;
BEGIN
    	
	DBMS_OUTPUT.PUT_LINE('== INICIO UDPATE {{info.schema_nome}} ==');

	{% for item in info.list -%}
BEGIN

		select count(1) INTO NCOUNT_ANTES 
		from  {{info.schema_nome}}.{{item.tabela_nome}} tab1 where {{item.where}};
		DBMS_OUTPUT.PUT_LINE('{{info.schema_nome}}.{{item.tabela_nome}}: {{item.matricula_origem}} -> '|| NCOUNT_ANTES || ' Registros encontrados');

		update {{info.schema_nome}}.{{item.tabela_nome}} tab1 set {{item.campo_nome}} = '{{item.matricula_destino}}' where {{item.where}};
	
		select count(1) INTO NCOUNT_DEPOIS 
		from  {{info.schema_nome}}.{{item.tabela_nome}} tab1 where {{item.where_depois}};

		DBMS_OUTPUT.PUT_LINE('{{info.schema_nome}}.{{item.tabela_nome}}: {{item.matricula_destino}} -> '|| NCOUNT_DEPOIS || ' Registros atualizados');

		COMMIT;
		DBMS_OUTPUT.PUT_LINE('COMMIT: OK');
	EXCEPTION  WHEN OTHERS THEN
		DBMS_OUTPUT.PUT_LINE('COMMIT: NOK');
		DBMS_OUTPUT.PUT_LINE('ERRO: '|| SQLERRM);
	END;

	{% endfor -%}

	DBMS_OUTPUT.PUT_LINE('== FIM {{info.schema_nome}} ==');

END;
/