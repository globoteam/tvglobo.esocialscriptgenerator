﻿{%  if {{info.schema_nome}} == 'HEJPSP' %} 
ALTER SESSION SET CURRENT_SCHEMA= HEJP
/
alter table HEJP.THEJPBC0 DISABLE constraint THEJPBC0_THEJFCN0_FK
/
alter table HEJP.THEJPGTBCO0 DISABLE constraint THEJPGTBCO0_THEJFCN0_FK
/
alter table HEJP.THEJSITBCO0 DISABLE constraint THEJSITBCO0_THEJFCN0_FK
/
alter table HEJP.THEJPBC0 DISABLE constraint SYS_C0019041
/
alter table HEJP.THEJSITBCO0 DISABLE constraint SYS_C0019079
/
alter table HEJP.THEJTRA0 DISABLE constraint SYS_C0019086
/
alter table HEJP.THEJSCP0 DISABLE constraint SYS_C0019068
/
alter table HEJP.THEJSISCPC0 DISABLE constraint SYS_C0019074
/
alter table HEJP.THEJJOR0_HIST DISABLE constraint SYS_C00143852
/
alter table HEJP.THEJAPR0_HIST DISABLE constraint SYS_C00143844
/
alter table HEJP.THEJAPR0_HIST DISABLE constraint IHEJAPR00HIST
/
alter table HEJP.THEJAPR2 DISABLE constraint SYS_C0018949
/
alter table HEJP.THEJAPR1 DISABLE constraint SYS_C0018942
/
alter table HEJP.THEJALOFCN0 DISABLE constraint SYS_C0018929
/
alter table HEJP.THEJAPR0 DISABLE constraint SYS_C0018934
/
alter table HEJP.THEJAPR0 DISABLE constraint IHEJAPR00
/
alter table HEJP.THEJFUNT DISABLE constraint SYS_C0019006
/
alter table HEJP.THEJFUNT DISABLE constraint IHEJFUNT0
/
alter table HEJP.THEJJOR0 DISABLE constraint SYS_C0019017
/
alter table HEJP.THEJJOR0 DISABLE constraint IHEJJOR00
/
alter table HEJP.THEJEAF0 DISABLE constraint SYS_C0018992
/
alter table HEJP.THEJFCN0 DISABLE constraint SYS_C0018997
/
alter table HEJP.THEJFCN0 DISABLE constraint IHEJFCN0
/
alter table HEJP.THEJALOFCN0 DISABLE constraint THEJALOFCN0_PK
/
alter table HEJP.THEJEAF0 DISABLE constraint THEJEAF0_PK
/
alter table HEJP.THEJPGTBCO0 DISABLE constraint THEJPGTBCO0_PK
/
alter table HEJP.THEJPBC0 DISABLE constraint THEJPBC0_PK
/
alter table HEJP.THEJSCP0 DISABLE constraint THEJSCP0_PK
/
alter table HEJP.THEJSISCPC0 DISABLE constraint THEJSISCPC0_PK
/
alter table HEJP.THEJPBC0 DISABLE constraint THEJPBC0_PK
/
alter table HEJP.THEJACS0 DISABLE constraint THEJACS0_PK
/
alter table HEJP.THEJATZ0 DISABLE constraint THEJATZ0_THEJACS0_FK
/
alter table HEJP.THEJATZ0 DISABLE constraint THEJATZ0_PK
/
alter table HEJP.THEJFUNT DISABLE constraint SYS_C0019006
/
alter table HEJP.THEJFUNT DISABLE constraint SYS_C0019007
/
alter table HEJP.THEJFUNT DISABLE constraint SYS_C0019008
/
alter table HEJP.THEJFUNT DISABLE constraint SYS_C0019009
/
alter table HEJP.THEJFUNT DISABLE constraint SYS_C0019010
/
alter table HEJP.THEJFUNT DISABLE constraint IHEJFUNT0
/

{% endif %}


ALTER SESSION SET CURRENT_SCHEMA= {{info.schema_nome}}
/
{% for item in info.list -%}
alter table {{info.schema_nome}}.{{item.tabela_nome}} DISABLE constraint {{item.constrainst_nome}}
/
{% endfor -%}



